﻿using ComartHeijn.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ComartHeijn.Data
{
	public class Seed
	{
		public static void SeedUsers(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
		{
			if (roleManager.FindByNameAsync("Admin").Result == null)
			{
				IdentityRole role = new IdentityRole { Name = "Admin" };
				roleManager.CreateAsync(role).Wait();
			}

			if (roleManager.FindByNameAsync("WebRedacteur").Result == null)
			{
				IdentityRole role = new IdentityRole { Name = "WebRedacteur" };
				roleManager.CreateAsync(role).Wait();
			}

			if (userManager.FindByEmailAsync("admin@admin.com").Result == null)
			{
				ApplicationUser user = new ApplicationUser
				{
					UserName = "admin@admin.com",
					Email = "admin@admin.com",
					EmailConfirmed = true
				};

				IdentityResult result = userManager.CreateAsync(user, "Password123!").Result;

				if (result.Succeeded)
				{
					userManager.AddToRoleAsync(user, "Admin").Wait();
				}

			}

			if (userManager.FindByEmailAsync("web@web.com").Result == null)
			{
				ApplicationUser user = new ApplicationUser
				{
					UserName = "web@web.com",
					Email = "web@web.com",
					EmailConfirmed = true
				};

				IdentityResult result = userManager.CreateAsync(user, "Password123!").Result;

				if (result.Succeeded)
				{
					userManager.AddToRoleAsync(user, "WebRedacteur").Wait();
				}

			}
		}
	}

}
