﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ComartHeijn.Models;

namespace ComartHeijn.Data
{
	public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
	{
		public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
			: base(options)
		{
		}

		protected override void OnModelCreating(ModelBuilder builder)
		{
			base.OnModelCreating(builder);
			builder.Entity<Product>()
			.HasOne(b => b.Category)
			.WithMany(a => a.Products)
			.OnDelete(DeleteBehavior.Restrict);

			builder.Entity<DeliverySlots>()
			.HasOne(d => d.Checkout)
			.WithMany(b => b.DeliverySlots);
		}

		public DbSet<ComartHeijn.Models.Product> Product { get; set; }
		public DbSet<ComartHeijn.Models.Category> Category { get; set; }
		public DbSet<ComartHeijn.Models.Subcategory> Subcategory { get; set; }
		public DbSet<ComartHeijn.Models.Subsubcategory> Subsubcategory { get; set; }
		public DbSet<ComartHeijn.Models.Discount> Discount { get; set; }
		public DbSet<ComartHeijn.Models.DeliverySlots> DeliverySlots { get; set; }
		public DbSet<ComartHeijn.Models.CartItem> CartItem { get; set; }
		public DbSet<ComartHeijn.Models.ApplicationUser> ApplicationUsers { get; set; }


		public DbSet<ComartHeijn.Models.NieuwsArtikel> NieuwsArtikel { get; set; }

		public DbSet<Order> Orders { get; set; }
		public DbSet<OrderLine> OrderLines { get; set; }

	}
}
