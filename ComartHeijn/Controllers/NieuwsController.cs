﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ComartHeijn.Data;
using ComartHeijn.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ComartHeijn.Controllers
{
    public class NieuwsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public NieuwsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Admin/Artikelen
        public IActionResult Index()
        {
            var artikelen = _context.NieuwsArtikel;
            List<NieuwsArtikel> nieuwsArtikels = new List<NieuwsArtikel>();

            foreach (NieuwsArtikel nieuwsArtikel in nieuwsArtikels)
            {
                NieuwsArtikel artl = new NieuwsArtikel
                {
                    NieuwsArtikelId = nieuwsArtikel.NieuwsArtikelId,
                    Titel = nieuwsArtikel.Titel,
                    Datum = nieuwsArtikel.Datum,
                    OmslagFoto = nieuwsArtikel.OmslagFoto
                };
                nieuwsArtikels.Add(artl);
            }

            return View(artikelen);
        }

        // GET: Admin/Artikelen/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var artikel = await _context.NieuwsArtikel
                .FirstOrDefaultAsync(m => m.NieuwsArtikelId == id);
            if (artikel == null)
            {
                return NotFound();
            }

            return View(artikel);
        }
    }
}