﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ComartHeijn.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ComartHeijn.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace ComartHeijn.Controllers
{
    public class ZoekresultatenController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ZoekresultatenController(ApplicationDbContext context)
        {
            _context = context;
        }

        //public async Task<IActionResult> Index()
        //{
        //    return View(await _context.Product.ToListAsync());
        //}
        //public async Task<IActionResult> Index(string productname)
        //{
        //    if (String.IsNullOrEmpty(productname))
        //    {
        //        var ApplicationDbContext = _context.Product.Include(p => p.Subsubcategory);
        //        return View(await _context.Product.ToListAsync());

        //    }

        //    else
        //    {
        //        var searchItems = await _context.Product.Include(p => p.Subsubcategory)
        //            .Where(s => s.Title.Contains(productname)).ToListAsync();
        //        return View(searchItems);
        //    }
        //}





        public async Task<IActionResult> Index(string searchString)
        {
            var products = from m in _context.Product
                           select m;

            if (!String.IsNullOrEmpty(searchString))
            {
                products = products.Where(s => s.Title.Contains(searchString));
            }

            return View(await products.ToListAsync());
        }


        // GET: Products/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Product
                .FirstOrDefaultAsync(m => m.Id == id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

    }
}
