﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ComartHeijn.Data;
using ComartHeijn.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace ComartHeijn.Controllers
{
    public class CheckoutController : Controller
    {
        private ApplicationDbContext _context;
        private UserManager<ApplicationUser> _userManager;

        public CheckoutController(ApplicationDbContext context,
            UserManager<ApplicationUser> userMan)
        {
            _context = context;
            _userManager = userMan;
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Index()
        {
            CheckoutViewModel cvm = new CheckoutViewModel();

            ApplicationUser user = await _userManager.GetUserAsync(HttpContext.User);

            cvm.Address = user.Straatnaam;
            cvm.City = user.Woonplaats;
            cvm.Housenumber = user.Huisnummer.ToString();
            cvm.Phonenumber = user.PhoneNumber;
            cvm.Zipcode = user.Postcode;
            List<DeliverySlots> deliverySlots = new List<DeliverySlots>();
            var deliveries = _context.DeliverySlots.ToList();
            cvm.DeliverySlots = deliveries;
            //ViewBag["DeliverySlot"] = new SelectList(cvm.DeliverySlots);





            List<CartItem> cart = new List<CartItem>();

            string cartString = HttpContext.Session.GetString("cart");
            if (cartString != null)
                cart = JsonConvert.DeserializeObject<List<CartItem>>(cartString);


            List<CartItemViewModel> cartvm = new List<CartItemViewModel>();

            double totalPrice = 0;

            foreach (CartItem ci in cart)
            {
                CartItemViewModel civm = new CartItemViewModel();

                civm.ProductId = ci.ProductId;
                civm.Amount = ci.Amount;

                Product p = _context.Product.Find(ci.ProductId);

                civm.Title = p.Title;
                civm.Price = Convert.ToDouble(p.Price);
                civm.Image = p.Image;

                totalPrice += ci.Amount * Convert.ToDouble(p.Price);

                cartvm.Add(civm);
            }

            cvm.CartItems = cartvm;
            cvm.TotalPrice = totalPrice;

            return View(cvm);
        }
        // GET: CheckOut/Delete/2
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var CartItem = await _context.CartItem
                .FirstOrDefaultAsync(m => m.ProductId == id);
            if (CartItem == null)
            {
                return NotFound();
            }

            return View("Index");
        }

        // POST: CheckOut/Delete/2
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var cartitem = await _context.CartItem.FindAsync(id);
            _context.CartItem.Remove(cartitem);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task<IActionResult> Index(CheckoutViewModel model)
        {
            Order order = new Order();
            order.Address = model.Address;
            order.City = model.City;
            order.Name = model.Name;
            order.Phonenumber = model.Phonenumber;
            order.Zipcode = model.Zipcode;
            order.Housenumber = model.Housenumber;

            ApplicationUser user = await _userManager.GetUserAsync(HttpContext.User);

            order.User = user;

            order.OrderLines = new List<OrderLine>();

            List<CartItem> cart = new List<CartItem>();

            string cartString = HttpContext.Session.GetString("cart");
            if (cartString != null)
                cart = JsonConvert.DeserializeObject<List<CartItem>>(cartString);

            foreach (CartItem cartItem in cart)
            {
                Product product = _context.Product.Find(cartItem.ProductId);

                OrderLine orderLine = new OrderLine();
                orderLine.Amount = cartItem.Amount;
                orderLine.Price = Convert.ToDouble(product.Price);
                orderLine.ProductId = cartItem.ProductId;

                order.OrderLines.Add(orderLine);
            }

            try
            {
                _context.Add(order);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return StatusCode(500);
            }

            return View("Confirm");


        }
        // Verwijder uit winkelwagen
        public IActionResult RemoveFromCart(int id)
        {
            List<CartItem> cart = new List<CartItem>();

            string cartString = HttpContext.Session.GetString("cart");
            if (cartString != null)
            {
                cart = JsonConvert.DeserializeObject<List<CartItem>>(cartString);
            }

            var product = cart.FirstOrDefault(p => p.ProductId == id);
            product.Amount--;

            if (product.Amount < 1)
            {
                cart.Remove(product);

            }
            cartString = JsonConvert.SerializeObject(cart);

            HttpContext.Session.SetString("cart", cartString);
            //Zorgt ervoor dat de klant niet kan bestellen zonder producten in zijn wagen.
            if (cart.Count < 1)
            {
                return RedirectToAction("Index", "Producten");

            }

            return RedirectToAction("Index");
        }
    }
}