﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ComartHeijn.Data;
using ComartHeijn.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using ComartHeijn.Migrations;

namespace ComartHeijn.Controllers
{
    public class ProductenController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ProductenController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Categories
        public async Task<IActionResult> Index()
        {
            return View(await _context.Category.ToListAsync());
        }

        [HttpGet]
        public IActionResult Categorie(int? id)
        {
            List<Product> producten = new List<Product>();
            var producties = _context.Product.ToList();
            var cat = _context.Category.ToList();

            foreach(var Producties in producties)
            {
                if(Producties.CategoryId == id){
                    var prod = Producties.Id;
                    var products = _context.Product.Where(a => a.Category.Id == id);
                    producten = products.ToList();
                }

            }            
            return View(producten);
        }

        // Aantal verhogen
        public IActionResult IncreaseAmount(int id) 
        {
            List<CartItem> cart = new List<CartItem>();

            string cartString = HttpContext.Session.GetString("cart");
            if (cartString != null) 
            {
                cart = JsonConvert.DeserializeObject<List<CartItem>>(cartString);
            }

            var product = cart.FirstOrDefault(p => p.ProductId == id);
            product.Amount++;

            cartString = JsonConvert.SerializeObject(cart);

            HttpContext.Session.SetString("cart", cartString);

            return RedirectToAction("ShowCart");
        }


        // Voeg toe aan winkelwagen
        public IActionResult AddToCart(int id)
        {
            List<CartItem> cart = new List<CartItem>();

            string cartString = HttpContext.Session.GetString("cart");
            if (cartString != null)
            {
                cart = JsonConvert.DeserializeObject<List<CartItem>>(cartString);
            }

            CartItem item = new CartItem { ProductId = id, Amount = 1 };
            cart.Add(item);

            cartString = JsonConvert.SerializeObject(cart);

            HttpContext.Session.SetString("cart", cartString);

            return RedirectToAction("Showcart");
        }

        // Verwijder uit winkelwagen
        public IActionResult RemoveFromCart(int id)
        {
            List<CartItem> cart = new List<CartItem>();

            string cartString = HttpContext.Session.GetString("cart");
            if (cartString != null)
            {
                cart = JsonConvert.DeserializeObject<List<CartItem>>(cartString);
            }

            var product = cart.FirstOrDefault(p => p.ProductId == id);
            product.Amount--;

            if (product.Amount < 1)
            {
                cart.Remove(product);
                
            }
            cartString = JsonConvert.SerializeObject(cart);

            //Zorgt ervoor dat de klant niet kan bestellen zonder producten in zijn wagen.
            if (cart.Count < 1)
            {
                return RedirectToAction("Index", "Producten");

            }

            HttpContext.Session.SetString("cart", cartString);

            return RedirectToAction("ShowCart");
        }
    

        // Laat winkelwagen zien
        public IActionResult ShowCart()
        {
            List<CartItem> cart = new List<CartItem>();

            string cartString = HttpContext.Session.GetString("cart");
            if (cartString != null)
            {
                cart = JsonConvert.DeserializeObject<List<CartItem>>(cartString);
            }

            List<CartItemViewModel> civm = new List<CartItemViewModel>();

            foreach (CartItem ci in cart)
            {

                
                Product product = _context.Product.Find(ci.ProductId);
                CartItemViewModel item = new CartItemViewModel();
        
                item.ProductId = ci.ProductId;
                item.Amount = ci.Amount;
                item.Title = product.Title;
                item.Image = product.Image;
                item.Price = Convert.ToDouble(product.Price);

                
                civm.Add(item);
            }

            return View(civm);
        }

        // GET: Products/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Product.Include(p => p.Category)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // GET: Products/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,EAN,Title,Brand,Shortdescription,Fulldescription,Price,Image,Weight,Category,Subcategory,Subsubcategory")] Product product)
        {
            if (ModelState.IsValid)
            {
                _context.Add(product);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(product);
        }

        // GET: Products/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Product.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }
            return View(product);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,EAN,Title,Brand,Shortdescription,Fulldescription,Price,Image,Weight,Subcategory,Subsubcategory")] Product product)
        {
            if (id != product.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(product);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductExists(product.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(product);
        }

        // GET: Products/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Product
                .FirstOrDefaultAsync(m => m.Id == id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var product = await _context.Product.FindAsync(id);
            _context.Product.Remove(product);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProductExists(int id)
        {
            return _context.Product.Any(e => e.Id == id);
        }
    }
}
