﻿using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ComartHeijn.Models;
using System.Xml.Linq;
using ComartHeijn.Data;
using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Xml;
using System.Collections.Generic;
using Microsoft.Graph;

namespace ComartHeijn.Controllers
{
	public class HomeController : Controller
	{
		private readonly ILogger<HomeController> _logger;
		private ApplicationDbContext _context;

		public HomeController(ILogger<HomeController> logger, ApplicationDbContext context)
		{
			_logger = logger;
			_context = context;
		}

		public IActionResult Index()
		{
			var model = new Home();
			model.Categories = _context.Category.ToList();
			model.Discounts = _context.Discount.ToList();
			model.Products = _context.Product.ToList();
			//Producten die in de aanbieding zijn laten zien.
			/*var producties = _context.Product.ToList();
			var disc = _context.Discount.ToList();
			
			foreach (var Producties in producties)
			{
				if (Producties.EAN == id)
				{
					var prod = Producties.Id;
					model.Discounts = _context.Discount.Where(a => a.EAN == id);
					
				}

			} */
			model.NieuwsArtikels = _context.NieuwsArtikel.ToList();
			return View(model);
		}

		// XML importeren
		public IActionResult ImportXml()
		{
			XDocument xdoc = XDocument.Load("https://supermaco.starwave.nl/api/products");
			XDocument xdoca = XDocument.Load("https://supermaco.starwave.nl/api/promotion");
			XDocument xdocd = XDocument.Load("https://supermaco.starwave.nl/api/deliveryslots");
			XDocument xdocc = XDocument.Load("https://supermaco.starwave.nl/api/categories");

			// Categorien importeren
			var categories = xdocc.Descendants("Category");
			var subcategories = xdocc.Descendants("Subcategory");
			var subsubcategories = xdocc.Descendants("Subsubcategory");


			// Zet categorien in database
			foreach (var categoryXml in categories)
			{
				Category category = new Category();

				category.Naam = categoryXml.Descendants("Name").First().Value;
				var catexist = _context.Category.FirstOrDefault(c => c.Naam == category.Naam);
				if (catexist == null)
				{
					_context.Add(category);
				}
				else
				{
					category = catexist;
				}

				foreach (var subcategoryXml in categoryXml.Descendants("Subcategory"))
				{
					Subcategory subcategory = new Subcategory();

					subcategory.Name = subcategoryXml.Descendants("Name").First().Value;
					var subcatexist = _context.Subcategory.FirstOrDefault(c => c.Name == subcategory.Name);
					if (subcatexist == null)
					{
						subcategory.Category = category;
						_context.Add(subcategory);
					}
					else
					{
						subcategory = subcatexist;
					}
				}

				_context.SaveChanges();

			}

			// Producten importeren
			var products = xdoc.Descendants("Product");

			// Zet producten in database
			foreach (var product in products)
			{
				Product p = new Product();

				p.EAN = product.Descendants("EAN").First().Value;
				p.Title = product.Descendants("Title").First().Value;
				p.Brand = product.Descendants("Brand").First().Value;
				p.Shortdescription = product.Descendants("Shortdescription").First().Value;
				p.Fulldescription = product.Descendants("Fulldescription").First().Value;
				p.Image = product.Descendants("Image").First().Value;
				p.Weight = product.Descendants("Weight").First().Value;
				p.Price = product.Descendants("Price").First().Value;


				var category = product.Descendants("Category").First().Value;
				var existing = _context.Category.FirstOrDefault(categories => categories.Naam == category);

				p.Category = existing;

				_context.Add(p);
			}


			// Aanbiedingen importeren


			var discounts = xdoca.Descendants("Discount");

			foreach (var discount in discounts)
			{
				Discount d = new Discount();

				d.EAN = discount.Descendants("EAN").First().Value;
				d.DiscountPrice = discount.Descendants("DiscountPrice").First().Value;
				d.ValidUntil = discount.Descendants("ValidUntil").First().Value;

				//var DiscPrice = discount.Descendants("DiscountPrice").First().Value;
				//var DiscountPrice = _context.Discount.FirstOrDefault(discounts => discounts.DiscountPrice == discount);

				//d.DiscountPrice = DiscountPrice;

				//var ValUntil = promotion.Descendants("ValidUntil").First().Value;
				//var ValidUntil = _context.Promotion.FirstOrDefault(promotions => promotions.ValidUntil == promotion);

				//d.ValidUntil = ValidUntil


				_context.Add(d);
			}

			//Bezorgtijden importeren

			var deliveryslots = xdocd.Descendants("Deliveryslot");

			//List<DeliverySlots> Starttime = new List<DeliverySlots>();
			//var Starttimes = _context.DeliverySlots.ToListAsync(Starttime);

			foreach (var deliveryslot in deliveryslots)
			{
				DeliverySlots d = new DeliverySlots();


				d.Date = deliveryslot.Descendants("Date").First().Value;

				//List<DeliverySlots> timeslots = deliveryslots.Elements("Timeslot").Select(sv => new DeliverySlots()
				//{
					//Starttime = (string)sv.Element("Starttime"),
					//Endtime = (string)sv.Element("Endtime"),
					//Price = (string)sv.Element("Price")
				//}).ToList();
				//List<DeliverySlots> Starttime = new List<DeliverySlots>();
				d.Starttime = deliveryslot.Descendants("StartTime").First().Value;
				d.Endtime = deliveryslot.Descendants("EndTime").First().Value;
				d.Price = deliveryslot.Descendants("Price").First().Value;

				_context.Add(d);

			}


			_context.SaveChanges();
			return View();
		}

		public IActionResult Privacy()
		{
			return View();
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}
	}
}