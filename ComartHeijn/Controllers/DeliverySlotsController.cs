﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ComartHeijn.Data;
using ComartHeijn.Models;

namespace ComartHeijn.Controllers
{
    public class DeliverySlotsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public DeliverySlotsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: DeliverySlots
        public async Task<IActionResult> Index()
        {
            return View(await _context.DeliverySlots.ToListAsync());
        }

        // GET: DeliverySlots/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var deliverySlots = await _context.DeliverySlots
                .FirstOrDefaultAsync(m => m.Id == id);
            if (deliverySlots == null)
            {
                return NotFound();
            }

            return View(deliverySlots);
        }

        // GET: DeliverySlots/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: DeliverySlots/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Date,Id,Starttime,Endtime,Price")] DeliverySlots deliverySlots)
        {
            if (ModelState.IsValid)
            {
                _context.Add(deliverySlots);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(deliverySlots);
        }

        // GET: DeliverySlots/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var deliverySlots = await _context.DeliverySlots.FindAsync(id);
            if (deliverySlots == null)
            {
                return NotFound();
            }
            return View(deliverySlots);
        }

        // POST: DeliverySlots/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Date,Id,Starttime,Endtime,Price")] DeliverySlots deliverySlots)
        {
            if (id != deliverySlots.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(deliverySlots);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DeliverySlotsExists(deliverySlots.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(deliverySlots);
        }

        // GET: DeliverySlots/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var deliverySlots = await _context.DeliverySlots
                .FirstOrDefaultAsync(m => m.Id == id);
            if (deliverySlots == null)
            {
                return NotFound();
            }

            return View(deliverySlots);
        }

        // POST: DeliverySlots/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var deliverySlots = await _context.DeliverySlots.FindAsync(id);
            _context.DeliverySlots.Remove(deliverySlots);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DeliverySlotsExists(int id)
        {
            return _context.DeliverySlots.Any(e => e.Id == id);
        }
    }
}
