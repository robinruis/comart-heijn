﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ComartHeijn.Migrations
{
    public partial class Subcategories : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SubcategoryId",
                table: "Category",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SubsubcategoryId",
                table: "Category",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Subcategory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subcategory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Subsubcategory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subsubcategory", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Category_SubcategoryId",
                table: "Category",
                column: "SubcategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Category_SubsubcategoryId",
                table: "Category",
                column: "SubsubcategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Category_Subcategory_SubcategoryId",
                table: "Category",
                column: "SubcategoryId",
                principalTable: "Subcategory",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Category_Subsubcategory_SubsubcategoryId",
                table: "Category",
                column: "SubsubcategoryId",
                principalTable: "Subsubcategory",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Category_Subcategory_SubcategoryId",
                table: "Category");

            migrationBuilder.DropForeignKey(
                name: "FK_Category_Subsubcategory_SubsubcategoryId",
                table: "Category");

            migrationBuilder.DropTable(
                name: "Subcategory");

            migrationBuilder.DropTable(
                name: "Subsubcategory");

            migrationBuilder.DropIndex(
                name: "IX_Category_SubcategoryId",
                table: "Category");

            migrationBuilder.DropIndex(
                name: "IX_Category_SubsubcategoryId",
                table: "Category");

            migrationBuilder.DropColumn(
                name: "SubcategoryId",
                table: "Category");

            migrationBuilder.DropColumn(
                name: "SubsubcategoryId",
                table: "Category");
        }
    }
}
