﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ComartHeijn.Migrations
{
    public partial class Bezorgmoment1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CheckoutName",
                table: "DeliverySlots",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "CheckoutViewModel",
                columns: table => new
                {
                    Name = table.Column<string>(nullable: false),
                    TotalPrice = table.Column<double>(nullable: false),
                    Address = table.Column<string>(nullable: false),
                    Housenumber = table.Column<string>(nullable: false),
                    Phonenumber = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: false),
                    Zipcode = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CheckoutViewModel", x => x.Name);
                });

            migrationBuilder.CreateTable(
                name: "CartItemViewModel",
                columns: table => new
                {
                    ProductId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Price = table.Column<double>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Image = table.Column<string>(nullable: true),
                    Amount = table.Column<int>(nullable: false),
                    CheckoutViewModelName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CartItemViewModel", x => x.ProductId);
                    table.ForeignKey(
                        name: "FK_CartItemViewModel_CheckoutViewModel_CheckoutViewModelName",
                        column: x => x.CheckoutViewModelName,
                        principalTable: "CheckoutViewModel",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DeliverySlots_CheckoutName",
                table: "DeliverySlots",
                column: "CheckoutName");

            migrationBuilder.CreateIndex(
                name: "IX_CartItemViewModel_CheckoutViewModelName",
                table: "CartItemViewModel",
                column: "CheckoutViewModelName");

            migrationBuilder.AddForeignKey(
                name: "FK_DeliverySlots_CheckoutViewModel_CheckoutName",
                table: "DeliverySlots",
                column: "CheckoutName",
                principalTable: "CheckoutViewModel",
                principalColumn: "Name",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DeliverySlots_CheckoutViewModel_CheckoutName",
                table: "DeliverySlots");

            migrationBuilder.DropTable(
                name: "CartItemViewModel");

            migrationBuilder.DropTable(
                name: "CheckoutViewModel");

            migrationBuilder.DropIndex(
                name: "IX_DeliverySlots_CheckoutName",
                table: "DeliverySlots");

            migrationBuilder.DropColumn(
                name: "CheckoutName",
                table: "DeliverySlots");
        }
    }
}
