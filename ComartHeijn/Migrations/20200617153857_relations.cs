﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ComartHeijn.Migrations
{
    public partial class relations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Category_Subcategory_SubcategoryId",
                table: "Category");

            migrationBuilder.DropIndex(
                name: "IX_Category_SubcategoryId",
                table: "Category");

            migrationBuilder.DropColumn(
                name: "SubcategoryId",
                table: "Category");

            migrationBuilder.AddColumn<int>(
                name: "CategoryId",
                table: "Subcategory",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Subcategory_CategoryId",
                table: "Subcategory",
                column: "CategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Subcategory_Category_CategoryId",
                table: "Subcategory",
                column: "CategoryId",
                principalTable: "Category",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Subcategory_Category_CategoryId",
                table: "Subcategory");

            migrationBuilder.DropIndex(
                name: "IX_Subcategory_CategoryId",
                table: "Subcategory");

            migrationBuilder.DropColumn(
                name: "CategoryId",
                table: "Subcategory");

            migrationBuilder.AddColumn<int>(
                name: "SubcategoryId",
                table: "Category",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Category_SubcategoryId",
                table: "Category",
                column: "SubcategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Category_Subcategory_SubcategoryId",
                table: "Category",
                column: "SubcategoryId",
                principalTable: "Subcategory",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
