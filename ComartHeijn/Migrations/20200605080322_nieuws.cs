﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ComartHeijn.Migrations
{
    public partial class nieuws : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "NieuwsArtikel",
                columns: table => new
                {
                    NieuwsArtikelId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Titel = table.Column<string>(nullable: true),
                    Content = table.Column<string>(nullable: true),
                    Datum = table.Column<DateTime>(nullable: false),
                    OmslagFoto = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NieuwsArtikel", x => x.NieuwsArtikelId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "NieuwsArtikel");
        }
    }
}
