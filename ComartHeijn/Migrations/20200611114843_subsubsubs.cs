﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ComartHeijn.Migrations
{
    public partial class subsubsubs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Subcategory",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "Subsubcategory",
                table: "Product");

            migrationBuilder.AddColumn<int>(
                name: "SubcategoryId",
                table: "Product",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SubsubcategoryId",
                table: "Product",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Product_SubcategoryId",
                table: "Product",
                column: "SubcategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Product_SubsubcategoryId",
                table: "Product",
                column: "SubsubcategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Product_Subcategory_SubcategoryId",
                table: "Product",
                column: "SubcategoryId",
                principalTable: "Subcategory",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Product_Subsubcategory_SubsubcategoryId",
                table: "Product",
                column: "SubsubcategoryId",
                principalTable: "Subsubcategory",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Product_Subcategory_SubcategoryId",
                table: "Product");

            migrationBuilder.DropForeignKey(
                name: "FK_Product_Subsubcategory_SubsubcategoryId",
                table: "Product");

            migrationBuilder.DropIndex(
                name: "IX_Product_SubcategoryId",
                table: "Product");

            migrationBuilder.DropIndex(
                name: "IX_Product_SubsubcategoryId",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "SubcategoryId",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "SubsubcategoryId",
                table: "Product");

            migrationBuilder.AddColumn<string>(
                name: "Subcategory",
                table: "Product",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Subsubcategory",
                table: "Product",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
