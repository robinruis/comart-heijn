﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ComartHeijn.Migrations
{
    public partial class Tamer2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LastName",
                table: "AspNetUsers");

            migrationBuilder.RenameColumn(
                name: "StraatNaam",
                table: "AspNetUsers",
                newName: "Straatnaam");

            migrationBuilder.RenameColumn(
                name: "PostCode",
                table: "AspNetUsers",
                newName: "Postcode");

            migrationBuilder.RenameColumn(
                name: "HuisNummer",
                table: "AspNetUsers",
                newName: "Huisnummer");

            migrationBuilder.AddColumn<string>(
                name: "Achternaam",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Voornaam",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Woonplaats",
                table: "AspNetUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Achternaam",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Voornaam",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Woonplaats",
                table: "AspNetUsers");

            migrationBuilder.RenameColumn(
                name: "Straatnaam",
                table: "AspNetUsers",
                newName: "StraatNaam");

            migrationBuilder.RenameColumn(
                name: "Postcode",
                table: "AspNetUsers",
                newName: "PostCode");

            migrationBuilder.RenameColumn(
                name: "Huisnummer",
                table: "AspNetUsers",
                newName: "HuisNummer");

            migrationBuilder.AddColumn<string>(
                name: "LastName",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
