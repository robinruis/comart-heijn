﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ComartHeijn.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string Naam { get; set; }
        public string BannerUrl { get; set; }
        public List<Product> Products { get; set; }
        public List<Subcategory> Subcategories { get; set; }
        public Subsubcategory Subsubcategory { get; set; }
        
    }
}
