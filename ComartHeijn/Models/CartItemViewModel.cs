﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ComartHeijn.Models
{
    public class CartItemViewModel
    {
        [Key]
        public int ProductId { get; set; }

        [Display(Name = "Prijs")]
        public double Price { get; set; }

        [Display(Name = "Naam")]
        public string Title { get; set; }

        [Display(Name = "Afbeelding")]
        public string Image { get; set; }

        [Display(Name = "Aantal")]
        public int Amount { get; set; }

        [Display(Name = "Totale prijs")]
        public double TotalPrice 
        {
            get 
            {
                return Price * Amount;
            }
        }
    }
}
