﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ComartHeijn.Models
{
    public class DeliverySlots
    {
        public int Id { get; set; }
        public string Date { get; set; }
        public string Timeslot { get; set; }
        public string Starttime { get; set; }
        public string Endtime { get; set; }
        public string Price { get; set; }
        public CheckoutViewModel Checkout { get; set; }
    }
}
