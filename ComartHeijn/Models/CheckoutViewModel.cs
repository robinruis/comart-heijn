﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ComartHeijn.Models
{
	public class CheckoutViewModel
	{
        public List<CartItemViewModel> CartItems { get; set; }
        public double TotalPrice { get; set; }
        [Key]
        [Required]
        [Display(Name = "Naam")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Straat")]
        public string Address { get; set; }

        [Required]
        [Display(Name = "Huisnummer")]
        public string Housenumber { get; set; }

        [Display(Name = "Telefoonnummer")]
        public string Phonenumber { get; set; }

        [Required]
        [Display(Name = "Woonplaats")]
        public string City { get; set; }

        [Required]
        [Display(Name = "Postcode")]
        public string Zipcode { get; set; }


        [Required]
        [Display(Name = "Bezorgmoment")]
        public List<DeliverySlots> DeliverySlots { get; set; }
    }
}
