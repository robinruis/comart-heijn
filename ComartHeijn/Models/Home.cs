﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ComartHeijn.Models
{
	public class Home
	{
		public IEnumerable<Category> Categories { get; set; }
		public IEnumerable<NieuwsArtikel> NieuwsArtikels { get; set; }
		public IEnumerable<Discount> Discounts { get; set; }
		public IEnumerable<Product> Products { get; set; }
	}
}
