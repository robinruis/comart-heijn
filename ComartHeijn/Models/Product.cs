﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Web;
using System.Xml;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ComartHeijn.Models
{
   
    public class Product
    {
        public int Id { get; set; }

        [Display(Name = "EAN code")]
        public string EAN { get; set; }

        [Display(Name = "Naam")]
        public string Title { get;  set; }

        [Display(Name = "Merk")]
        public string Brand { get;  set; }

        [Display(Name = "Korte beschrijving")]
        public string Shortdescription { get;  set; }

        [Display(Name = "Lange beschrijving")]
        public string Fulldescription { get;  set; }

        [Display(Name = "Prijs")]
        public string Price { get;  set; }

        [Display(Name = "Afbeelding")]
        public string Image { get;  set; }

        [Display(Name = "Gewicht")]
        public string Weight { get;  set; }

        [Display(Name = "Categorie")]
        public Category Category { get; set; }
        public int CategoryId { get; set; }

        [Display(Name = "Sub-categorie")]
        public Subcategory Subcategory { get;  set; }

        [Display(Name = "Sub-sub-categorie")]
        public Subsubcategory Subsubcategory { get;  set; }
    }
}
