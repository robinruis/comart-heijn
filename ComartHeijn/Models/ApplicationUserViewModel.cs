﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ComartHeijn.Models
{
    public class ApplicationUserViewModel
    {
            public string Id { get; set; }
            public string Email { get; set; }
            public IList<string> Roles { get; set; }
        }
}
