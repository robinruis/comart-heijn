﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ComartHeijn.Models
{
	public class Order
	{
		public int Id { get; set; }
		[Required]
		public string Name { get; set; }
		[Required]
		public string Address { get; set; }
		[Required]
		public string City { get; set; }

		public DateTime OrderDate { get; set; }

		public List<OrderLine> OrderLines { get; set; }

		public ApplicationUser User { get; set; }
		public string UserId { get; set; }

		public string Housenumber { get; set; }

		public string Phonenumber { get; set; }

		public string Zipcode { get; set; }

		public Order()
		{
			OrderDate = DateTime.Now;
		}


	}
}
