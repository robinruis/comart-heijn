﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ComartHeijn.Models
{
    public class CartItem
    {
        public int Amount { get; set; }
        [Key]
        public int ProductId { get; set; }
    }
}
