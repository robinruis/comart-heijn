﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ComartHeijn.Models
{
	public class NieuwsArtikel
	{
		public int NieuwsArtikelId { get; set; }
		public string Titel { get; set; }
		public string Content { get; set; }
		public DateTime Datum { get; set; } = DateTime.Now;
		public string OmslagFoto { get; set; }
	}
}
