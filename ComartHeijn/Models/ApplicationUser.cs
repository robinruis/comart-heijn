﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ComartHeijn.Models
{
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser() : base()
        {

        }
        public string Voornaam { get; set; }
        public string Achternaam { get; set; }
        public string Woonplaats { get; set; }
        public string Postcode { get; set; }
        public string Straatnaam { get; set; }
        public int Huisnummer { get; set; }
    }
}
