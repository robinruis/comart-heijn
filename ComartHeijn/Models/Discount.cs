﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ComartHeijn.Models
{
    public class Discount
    {
        [Display(Name = "Promotie")]
        public string Promotion { get; set; }
        public int Id { get; set; }

        [Display(Name = "EAN code")]
        public string EAN { get; set; }

        [Display(Name = "Korting prijs")]
        public string DiscountPrice { get; set; }

        [Display(Name = "Geldig tot ")]
        public string ValidUntil { get; set; }
    }
}
