﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ComartHeijn.Models
{
    public class User
    {
        public IdentityUser Id { get; set; }
        public IdentityUser Email { get; set; }
        public IdentityUser Roles { get; set; }
    }
}
