﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ComartHeijn.Data;
using ComartHeijn.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ComartHeijn.Areas.Admin.Controllers
{
    
        [Authorize]
        [Area("Admin")]
        public class NieuwsBeheerController : Controller
        {
            private readonly ApplicationDbContext _context;
            private readonly IWebHostEnvironment _iweb;

            public NieuwsBeheerController(ApplicationDbContext context, IWebHostEnvironment iweb)
            {
                _context = context;
                _iweb = iweb;
            }

            // GET: Admin/Artikelen
            [Authorize(Roles = "Admin, WebRedacteur")]
            [Area("Admin")]
            public IActionResult Index()
            {
                var artikelen = _context.NieuwsArtikel;
                List<NieuwsArtikel> artikels = new List<NieuwsArtikel>();

                foreach (NieuwsArtikel artikel in artikels)
                {
                    NieuwsArtikel artl = new NieuwsArtikel
                    {
                        NieuwsArtikelId = artikel.NieuwsArtikelId,
                        Titel = artikel.Titel,
                        Datum = artikel.Datum,
                        OmslagFoto = artikel.OmslagFoto
                    };
                    artikels.Add(artl);
                }

                return View(artikelen);
            }

            // GET: Admin/Artikelen/Details/5
            [Authorize(Roles = "Admin, WebRedacteur")]
            [Area("Admin")]
            public async Task<IActionResult> Details(int? id)
            {
                if (id == null)
                {
                    return NotFound();
                }

                var artikel = await _context.NieuwsArtikel
                    .FirstOrDefaultAsync(m => m.NieuwsArtikelId == id);
                if (artikel == null)
                {
                    return NotFound();
                }

                return View(artikel);
            }

            // GET: Admin/Artikelen/Create
            [Authorize(Roles = "Admin, WebRedacteur")]
            [Area("Admin")]
            public IActionResult NieuwArtikel()
            {
                return View();
            }

            // POST: Admin/Artikelen/Create
            // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
            // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
            [HttpPost]
            [ValidateAntiForgeryToken]
            [Authorize(Roles = "Admin, WebRedacteur")]
            [Area("Admin")]
            public async Task<IActionResult> NieuwArtikel([Bind("NieuwsArtikelId,Titel,Content,Datum,OmslagFoto")] NieuwsArtikel artikel, IFormFile fileobj)
            {
                if (ModelState.IsValid)
                {

                    string imgtxt = Path.GetExtension(fileobj.FileName);
                    string file_name = artikel.NieuwsArtikelId.ToString();
                    if (imgtxt == ".jpg" || imgtxt == ".png")
                    {
                        var uploadimg = Path.Combine(_iweb.WebRootPath, "Omslagfoto", fileobj.FileName);
                        var fileStream = new FileStream(uploadimg, FileMode.Create);
                        await fileobj.CopyToAsync(fileStream);

                        artikel.OmslagFoto = fileobj.FileName;
                        _context.Add(artikel);
                        await _context.SaveChangesAsync();
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ViewBag.message = "Alleen .jpg of .png afbeeldingen toegestaan.";
                    }
                }

                return View(artikel);
            }

            // GET: Admin/Artikelen/Edit/5
            [Authorize(Roles = "Admin, WebRedacteur")]
            [Area("Admin")]
            public async Task<IActionResult> Wijzigen(int? id)
            {
                if (id == null)
                {
                    return NotFound();
                }

                var artikel = await _context.NieuwsArtikel.FindAsync(id);
                if (artikel == null)
                {
                    return NotFound();
                }

                return View(artikel);
            }

            // POST: Admin/Artikelen/Edit/5
            // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
            // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
            [HttpPost]
            [ValidateAntiForgeryToken]
            [Authorize(Roles = "Admin, WebRedacteur")]
            [Area("Admin")]
            public async Task<IActionResult> Wijzigen(int id, [Bind("NieuwsArtikelId,Titel,Content,OmslagFoto")] NieuwsArtikel artikel, IFormFile fileobj)
            {
                if (id != artikel.NieuwsArtikelId)
                {
                    return NotFound();
                }

                if (ModelState.IsValid)
                {
                try
                {
                    string imgtxt = Path.GetExtension(fileobj.FileName);
                    string file_name = artikel.NieuwsArtikelId.ToString();
                    if (imgtxt == ".jpg" || imgtxt == ".png")
                    {
                        var uploadimg = Path.Combine(_iweb.WebRootPath, "Omslagfoto", fileobj.FileName);
                        var fileStream = new FileStream(uploadimg, FileMode.Create);
                        await fileobj.CopyToAsync(fileStream);

                        artikel.OmslagFoto = fileobj.FileName;
                        _context.Update(artikel);
                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                        ViewBag.message = "Alleen .jpg of .png afbeeldingen toegestaan.";
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ArtikelExists(artikel.NieuwsArtikelId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                    return RedirectToAction(nameof(Index));
                }

                return View(artikel);
            }

            // GET: Admin/Artikelen/Delete/5
            [Authorize(Roles = "Admin, WebRedacteur")]
            [Area("Admin")]
            public async Task<IActionResult> Verwijderen(int? id)
            {
                if (id == null)
                {
                    return NotFound();
                }

                var artikel = await _context.NieuwsArtikel

                    .FirstOrDefaultAsync(m => m.NieuwsArtikelId == id);
                if (artikel == null)
                {
                    return NotFound();
                }

                return View(artikel);
            }

            // POST: Admin/Artikelen/Delete/5
            [HttpPost]
            [ValidateAntiForgeryToken]
            [Authorize(Roles = "Admin, WebRedacteur")]
            [Area("Admin")]
            public async Task<IActionResult> Verwijderen(int id)
            {
                var artikel = await _context.NieuwsArtikel.FindAsync(id);
                _context.NieuwsArtikel.Remove(artikel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            [Authorize(Roles = "Admin, WebRedacteur")]
            [Area("Admin")]
            private bool ArtikelExists(int id)
            {
                return _context.NieuwsArtikel.Any(e => e.NieuwsArtikelId == id);
            }
        }
    }