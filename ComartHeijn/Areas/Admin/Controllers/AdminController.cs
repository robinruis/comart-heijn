﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ComartHeijn.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ComartHeijn.Areas.Admin.Controllers
{
    public class AdminController : Controller
    {
        private readonly ApplicationDbContext _context;

        public AdminController(ApplicationDbContext context)
        {
            _context = context;
        }

        [Area("Admin")]

        [Authorize(Roles = "Admin, WebRedacteur")]
        public IActionResult Index()
        {
            return View();
        }


    }
}