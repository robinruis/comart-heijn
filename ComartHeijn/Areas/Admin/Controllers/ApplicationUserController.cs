﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ComartHeijn.Data;
using ComartHeijn.Models;

namespace ComartHeijn.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class UsersController : Controller
    {
        private readonly ApplicationDbContext _context;

        UserManager<IdentityUser> _userManager;
        RoleManager<IdentityRole> _roleManager;

        public UsersController(UserManager<IdentityUser> userManager, ApplicationDbContext context, RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _context = context;
        }

        public IActionResult Index()
        {
            var users = _userManager.Users;

            List<ApplicationUserViewModel> userVM = new List<ApplicationUserViewModel>();

            foreach (IdentityUser user in users)
            {
                ApplicationUserViewModel uvm = new ApplicationUserViewModel();
                uvm.Id = user.Id;
                uvm.Email = user.Email;
                uvm.Roles = _userManager.GetRolesAsync(user).Result;

                userVM.Add(uvm);
            }

            return View(userVM);
        }

        public IActionResult Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = _context.Users.FirstOrDefaultAsync(m => m.Id == id).Result;

            List<ApplicationUserViewModel> userVM = new List<ApplicationUserViewModel>();

            ApplicationUserViewModel uvm = new ApplicationUserViewModel();
            uvm.Id = user.Id;
            uvm.Email = user.Email;
            uvm.Roles = _userManager.GetRolesAsync(user).Result;

            userVM.Add(uvm);


            return View(userVM);
        }

        public async Task<IActionResult> ModifyRoles(string id)
        {
            var user = _userManager.FindByIdAsync(id).Result;

            ApplicationUserViewModel uvm = new ApplicationUserViewModel();
            uvm.Id = user.Id;
            uvm.Roles = _userManager.GetRolesAsync(user).Result;

            ViewBag.Roles = _roleManager.Roles;

            return View(uvm);
        }

        [HttpPost]
        public async Task<IActionResult> ModifyRoles(string id, string[] SelectedRoles)
        {
            var user = _userManager.FindByIdAsync(id).Result;

            string[] allRoles = _roleManager.Roles.Select(r => r.Name).ToArray();
            await _userManager.RemoveFromRolesAsync(user, allRoles);

            foreach (string role in SelectedRoles)
            {
                await _userManager.AddToRoleAsync(user, role);
            }

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> DeleteAsync(string id)
        {
            var user = _userManager.FindByIdAsync(id).Result;
            await _userManager.DeleteAsync(user);

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var users = await _context.Users.FindAsync(id);
            if (users == null)
            {
                return NotFound();
            }
            return View(users);
        }

        // POST: Admin/Consultants/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(string id, [Bind("Id,Email,Roles")] ApplicationUserViewModel users)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(users);
                    _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                }
                return RedirectToAction(nameof(Index));
            }
            return View(users);
        }

        private bool UserViewModelExists(string id)
        {
            return _context.Users.Any(e => e.Id == id);
        }
    }
}