﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ComartHeijn.Data;
using ComartHeijn.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ComartHeijn.Areas.Admin.Controllers
{
    public class CategorieBannersController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IWebHostEnvironment _iweb;

        public CategorieBannersController(ApplicationDbContext context, IWebHostEnvironment iweb)
        {
            _context = context;
            _iweb = iweb;
        }

        [Area("Admin")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Index()
        {
            return View(await _context.Category.ToListAsync());
        }

        [Area("Admin")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Wijzig(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var category = await _context.Category.FindAsync(id);

            return View(category);
        }

        [Area("Admin")]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Wijzig(int id, [Bind("Id,Naam,BannerUrl")] Category category, IFormFile fileobj)
        {
            if (id != category.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    string imgtxt = Path.GetExtension(fileobj.FileName);
                    string file_name = category.Id.ToString();
                    if (imgtxt == ".jpg" || imgtxt == ".png")
                    {
                        var uploadimg = Path.Combine(_iweb.WebRootPath, "Banners", fileobj.FileName);
                        var fileStream = new FileStream(uploadimg, FileMode.Create);

                        await fileobj.CopyToAsync(fileStream);
                        category.BannerUrl = fileobj.FileName;
                        _context.Update(category);
                        await _context.SaveChangesAsync();

                        ViewBag.message = "Succesvol opgeslagen.";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ViewBag.message = "Alleen .jpg of .png afbeeldingen toegestaan.";
                    }
                    
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CategoryExists(category.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            return View(category);
        }

        private bool CategoryExists(int id)
        {
            return _context.Category.Any(e => e.Id == id);
        }

    }
}